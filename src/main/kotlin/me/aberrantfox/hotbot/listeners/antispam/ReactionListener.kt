package me.aberrantfox.hotbot.listeners.antispam

import me.aberrantfox.hotbot.extensions.jda.fullName
import me.aberrantfox.hotbot.logging.BotLogger
import me.aberrantfox.hotbot.services.Configuration
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.MessageReaction
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent
import net.dv8tion.jda.core.events.message.react.MessageReactionRemoveEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class ReactionListener(val config: Configuration, val log: BotLogger, private val mutedRole: Role) : ListenerAdapter() {
    override fun onMessageReactionAdd(event: MessageReactionAddEvent?) {
        if (event != null) handleReaction(event.member, event.reactionEmote, event.reaction, event.channel, "added")
    }

    override fun onMessageReactionRemove(event: MessageReactionRemoveEvent?) {
        if (event != null) handleReaction(event.member, event.reactionEmote, event.reaction, event.channel, "removed")
    }

    private fun handleReaction(author: Member?, emote: MessageReaction.ReactionEmote, reaction: MessageReaction, channel: MessageChannel, verb: String) {
        if (author == null || author.user.isBot) return

        val id = author.user.id

        if (author.roles.contains(mutedRole)) {
            reaction.removeReaction(author.user).queue()
            log.alert("${author.fullName()} reacted using ${emote.name} while muted and it has been removed.")
        }

        if (config.security.verboseLogging) {
            log.alert("${author.fullName()} (id: $id) $verb the emote \"${emote.name}\" in #${channel.name} (message: ${reaction.messageId}).")
        }
    }
}
